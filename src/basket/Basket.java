package basket;

import basket.fruit.Fruit;
import basket.iterator.FruitIterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Igor on 14.05.2016.
 */
public class Basket {
    private List<Fruit> fruits;

    public Basket() {
        fruits = new ArrayList<>();
    }
    public void addFruit(Fruit fruit){
        fruits.add(fruit);
    }
    public FruitIterator getFruitIterator(){
        return new BasketIterator(fruits);
    }
}
class BasketIterator implements FruitIterator{
    private List<Fruit> fruits;
    private int current = 0;
    private final int size;
    private boolean end = false;

    public BasketIterator(List<Fruit> fruits) {
        this.fruits = fruits;
        size = fruits.size();
    }

    @Override
    public Fruit getNext() {
        //System.out.println("Current = " + current);
        int now = current;
        if ( current == size / 2){
            end = true;
        }
        if ( current < size / 2){
            current = (size - current) - 1;
        }
        else{
            current = size - current;
        }
        return fruits.get(now);
    }

    @Override
    public boolean hasNext() {
        return !end;
    }
}
