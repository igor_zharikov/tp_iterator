package basket.iterator;

import basket.fruit.Fruit;

/**
 * Created by Igor on 14.05.2016.
 */
public interface FruitIterator {
    Fruit getNext();
    boolean hasNext();
}
