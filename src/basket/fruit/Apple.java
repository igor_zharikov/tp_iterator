package basket.fruit;

/**
 * Created by Igor on 14.05.2016.
 */
public class Apple implements Fruit {
    @Override
    public String getName() {
        return "Apple";
    }
}
