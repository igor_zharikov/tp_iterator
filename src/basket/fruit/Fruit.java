package basket.fruit;

/**
 * Created by Igor on 14.05.2016.
 */
public interface Fruit {
    String getName();
}
