package basket.fruit;

/**
 * Created by Igor on 14.05.2016.
 */
public class Orange implements Fruit {
    @Override
    public String getName() {
        return "Orange";
    }
}
