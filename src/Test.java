import basket.Basket;
import basket.fruit.Apple;
import basket.fruit.Banana;
import basket.fruit.Fruit;
import basket.fruit.Orange;
import basket.iterator.FruitIterator;

/**
 * Created by Igor on 14.05.2016.
 */
public class Test {
    public static void main(String... args){
        Basket basket = new Basket();
        basket.addFruit(new Apple());
        basket.addFruit(new Orange());
        basket.addFruit(new Banana());
        basket.addFruit(new Banana());
        basket.addFruit(new Orange());
        basket.addFruit(new Apple());
        FruitIterator iterator = basket.getFruitIterator();
        while(iterator.hasNext()){
            System.out.println(iterator.getNext().getName());
        }
    }
}
